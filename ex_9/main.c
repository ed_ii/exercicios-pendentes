#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 100

typedef struct registro {
    char chave[50];
    char valor[50];
    struct registro *prox;
} Registro;

typedef struct tabelaHash {
    Registro *tabela[MAX_SIZE];
} TabelaHash;

int funcaoHash(char *chave) {
    int i, soma = 0;
    for (i = 0; i < strlen(chave); i++) {
        soma += chave[i];
    }
    return soma % MAX_SIZE;
}

void inicializar(TabelaHash *t) {
    int i;
    for (i = 0; i < MAX_SIZE; i++) {
        t->tabela[i] = NULL;
    }
}

void inserir(TabelaHash *t, char *chave, char *valor) {
    int indice = funcaoHash(chave);
    Registro *novo = (Registro*) malloc(sizeof(Registro));
    if (novo == NULL) {
        printf("Erro ao alocar memoria.\n");
        exit(1);
    }
    strcpy(novo->chave, chave);
    strcpy(novo->valor, valor);
    novo->prox = NULL;
    if (t->tabela[indice] == NULL) {
        t->tabela[indice] = novo;
    } else {
        Registro *atual = t->tabela[indice];
        Registro *anterior = NULL;
        while (atual != NULL) {
            anterior = atual;
            atual = atual->prox;
        }
        anterior->prox = novo;
    }
}

void imprimir(TabelaHash *t) {
    int i;
    for (i = 0; i < MAX_SIZE; i++) {
        Registro *atual = t->tabela[i];
        printf("tabela[%d]:", i);
        while (atual != NULL) {
            printf(" [%s,%s]", atual->chave, atual->valor);
            atual = atual->prox;
        }
        printf("\n");
    }
}

int buscar(TabelaHash *t, char *chave, char *valor) {
    int indice = funcaoHash(chave);
    Registro *atual = t->tabela[indice];
    while (atual != NULL) {
        if (strcmp(atual->chave, chave) == 0 && strcmp(atual->valor, valor) == 0) {
            return 1;
        }
        atual = atual->prox;
    }
    return 0;
}

void deletar(TabelaHash *t, char *chave, char *valor) {
    int indice = funcaoHash(chave);
    Registro *atual = t->tabela[indice];
    Registro *anterior = NULL;

    while (atual != NULL) {
        if (strcmp(atual->chave, chave) == 0 && strcmp(atual->valor, valor) == 0) {
            if (anterior == NULL) {
                t->tabela[indice] = atual->prox;
            } else {
                anterior->prox = atual->prox;
            }
            free(atual);
            printf("Registro deletado.\n");
            return;
        }
        anterior = atual;
        atual = atual->prox;
    }
    printf("Registro nao encontrado.\n");
}

int main() {
    TabelaHash t;
    inicializar(&t);

    // inserindo valores na tabela hash
    inserir(&t, "chave1", "valor1");
    inserir(&t, "chave1", "valor7");
    inserir(&t, "chave1", "valor8");
    inserir(&t, "chave2", "valor2");
    inserir(&t, "chave3", "valor3");
    inserir(&t, "chave4", "valor4");
    inserir(&t, "chave5", "valor5");
    inserir(&t, "chave6", "valor6");

    imprimir(&t);

    printf("\n\n----------------------------------\n\n");

    // deletando valores na tabela hash
    deletar(&t, "chave1", "valor1");

    imprimir(&t);

    //buscando valor deletado na tabela hash
    if (buscar(&t, "chave1", "valor1")) {
        printf("Registro encontrado.\n");
    } else {
        printf("Registro nao encontrado.\n");
    }


    // buscando valor existente na tabela hash
    if (buscar(&t, "chave3", "valor3")) {
        printf("Registro encontrado.\n");
    } else {
        printf("Registro nao encontrado.\n");
    }

    return 0;
}