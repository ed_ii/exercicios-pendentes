#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

// Função de identificação de nó do professor
int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

void rotacionar_esquerda(ArvoreRB ** raiz, ArvoreRB * no){
  ArvoreRB * direita = no->dir;
  no->dir = direita->esq;
  direita->esq = no;
  direita->cor = no->cor;
  no->cor = RED;
  if(no == *raiz){
    *raiz = direita;
  }
}

void rotacionar_direita(ArvoreRB ** raiz, ArvoreRB * no){
  ArvoreRB * esquerda = no->esq;
  no->esq = esquerda->dir;
  esquerda->dir = no;
  esquerda->cor = no->cor;
  no->cor = RED;
  if(no == *raiz){
    *raiz = esquerda;
  }
}

void trocar_cores(ArvoreRB * no){
  no->cor = RED;
  no->esq->cor = BLACK;
  no->dir->cor = BLACK;
}

// funções para inserção
ArvoreRB * inserir_rec(ArvoreRB * raiz, int valor){
  if(!raiz){
    raiz = (ArvoreRB *) malloc(sizeof(ArvoreRB));
    raiz->info = valor;
    raiz->cor = RED;
    raiz->esq = NULL;
    raiz->dir = NULL;
    return raiz;
  }

  if(valor < raiz->info){
    raiz->esq = inserir_rec(raiz->esq, valor);
  }
  else if(valor > raiz->info){
    raiz->dir = inserir_rec(raiz->dir, valor);
  }
  else{
    return raiz;
  }

  if(eh_no_vermelho(raiz->dir) && !eh_no_vermelho(raiz->esq)){
    rotacionar_esquerda(&raiz, raiz);
  }
  if(eh_no_vermelho(raiz->esq) && eh_no_vermelho(raiz->esq->esq)){
    rotacionar_direita(&raiz, raiz);
  }
  if(eh_no_vermelho(raiz->esq) && eh_no_vermelho(raiz->dir)){
    trocar_cores(raiz);
  }

  return raiz;
}

ArvoreRB * inserir(ArvoreRB * raiz, int valor){
  raiz = inserir_rec(raiz, valor);
  raiz->cor = BLACK;
  return raiz;
}


// função para remoção considerando as três possibilidades de nó
ArvoreRB * remover(ArvoreRB * raiz, int valor){
  if(!raiz) {
    return raiz;
  }

  if(valor < raiz->info){
    raiz->esq = remover(raiz->esq, valor);
  }
  else if(valor > raiz->info){
    raiz->dir = remover(raiz->dir, valor);
  }
  else{
    if(!raiz->esq && !raiz->dir){ // Caso 1: nó folha
      free(raiz);
      return NULL;
    }
    else if(!raiz->esq || !raiz->dir){ // Caso 2: nó com um filho
      ArvoreRB * filho = raiz->esq ? raiz->esq : raiz->dir;
      *raiz = *filho;
      free(filho);
    }
    else{ // Caso 3: nó com dois filhos
      ArvoreRB * sucessor = raiz->dir;
      while(sucessor->esq){
        sucessor = sucessor->esq;
      }
      raiz->info = sucessor->info;
      raiz->dir = remover(raiz->dir, sucessor->info);
    }
  }

  // ajuste das cores e rotações para manter as propriedades da árvore red-black
  if(eh_no_vermelho(raiz->dir) && !eh_no_vermelho(raiz->esq)){
    rotacionar_esquerda(&raiz, raiz);
  }
  if(eh_no_vermelho(raiz->esq) && eh_no_vermelho(raiz->esq->esq)){
    rotacionar_direita(&raiz, raiz);
  }
  if(eh_no_vermelho(raiz->esq) && eh_no_vermelho(raiz->dir)){
    trocar_cores(raiz);
  }

  return raiz;
}


// funções do professor
int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}

void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

void print(ArvoreRB * a,int spaces){
  int i;
  for(i=0;i<spaces;i++) printf(" ");
  if(!a){
    printf("//\n");
    return;
  }

  printf("%d\n", a->info);
  print(a->esq,spaces+2);
  print(a->dir,spaces+2);
}

int main(){
  ArvoreRB * a = NULL;

  // Inserindo nós na árvore
  a = inserir(a, 50);
  a = inserir(a, 30);
  a = inserir(a, 90);
  a = inserir(a, 20);
  a = inserir(a, 40);
  a = inserir(a, 95);
  a = inserir(a, 10);
  a = inserir(a, 35);
  a = inserir(a, 45);
  a = inserir(a, 37);

  // Imprimindo a árvore
  printf("Imprimindo a arvore gerada");
  printf("\n");
  print(a, 0);
  printf("\n");

  // Removendo alguns nós da árvore
  a = remover(a, 45);
  a = remover(a, 50);
  a = remover(a, 90);

  // Imprimindo a arvore após as remoções
  printf("Imprimindo a arvore apos as remocoes");
  printf("\n");
  print(a, 0);
  printf("\n");

  return 0;
}
