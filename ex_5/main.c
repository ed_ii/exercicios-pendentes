#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void merge_sort(int *arr, int esquerda, int direita);
void insertion_sort(int *v, int n);

int main()
{
    int i, n;
    int *arr;

    // Definindo o tamanho da lista a ser ordenada
    n = 50;

    arr = (int *)malloc(sizeof(int) * n);

    // Preenchendo a lista com números aleatórios
    srand(time(NULL));
    for (i = 0; i < n; i++)
        arr[i] = rand() % 100;

    printf("Lista antes da ordenacao:\n");
    for (i = 0; i < n; i++)
        printf("%d ", arr[i]);
    printf("\n------------\n\n");

    // Ordena a lista usando o mergesort
    merge_sort(arr, 0, n - 1);

    // Imprime a lista depois da ordenação
    printf("Lista depois da ordenacao:\n");
    for (i = 0; i < n; i++)
        printf("%d ", arr[i]);
    printf("\n------------\n");

    free(arr);

    return 0;
}


void insertion_sort(int *v, int n)
{
    int i, j, x;
        
    for (i = 1; i < n; i++)
    {
        x = v[i];
        j = i - 1;
        while (j >= 0 && v[j] > x)
        {
        v[j + 1] = v[j];
        j--;
        }
        v[j + 1] = x;
    }
}

void merge_sort(int *arr, int esquerda, int direita)
{
    int meio, i, j, k;
    int *aux;

    if (direita <= esquerda)
        return;

    meio = (esquerda + direita) / 2;
    merge_sort(arr, esquerda, meio);
    merge_sort(arr, meio + 1, direita);

  // Aloca memória para a lista auxiliar
    aux = (int *)malloc(sizeof(int) * (direita - esquerda + 1));

  // Copia a primeira metade do array para a lista auxiliar
    for (i = meio; i >= esquerda; i--)
        aux[i - esquerda] = arr[i];

  // Copia a segunda metade do array para a lista auxiliar
    for (j = meio + 1; j <= direita; j++)
        aux[direita - j + meio + 1 - esquerda] = arr[j];

  // Verificando se não é melhor usar o insertion sort por conta da lista ser pequena
    if (direita - esquerda + 1 <= 10)
    {
    insertion_sort(aux, direita - esquerda + 1);
    for (k = esquerda; k <= direita; k++)
        arr[k] = aux[k - esquerda];
    }
    else
    {
    // Realiza a mesclagem das duas metades ordenadas
    i = 0;
    j = direita - esquerda;
    for (k = esquerda; k <= direita; k++)
    {
        if (aux[i] < aux[j])
        arr[k] = aux[i++];
        else
        arr[k] = aux[j--];
    }
    }

  // Libera a memória alocada para a lista auxiliar
  free(aux);
}


